// Conexion a la base de datos 
import mysql from 'mysql';
import keys from './keys';

const pool = mysql.createPool(keys.database); //Crear el modulo de conexion a la base de datos
pool.getConnection(function (err, connection) {
    if (err) {
        switch (err.code) {
            case "PROTOCOL_CONNECTION_LOST":
                console.error('Conexion Perdida');
                break;
            case "ECONNREFUSED":
                console.error("Conexion Rechazada");
                break;
            case "ER_ACCESS_DENIED_ERROR":
                console.error('Error de credenciales');
                break;
            case "ER_CON_COUNT_ERROR":
                console.error('Demasiadas conexion');
                break;
            case "ECONNRESET":
                console.error('No se obtuvo respuesta del servidor');
                break;
        }
    }
    if (connection) {
        connection.release();
        console.log('DB CONECTADA');
    }
    return;
});
export default pool;