import jwt from 'jwt-simple';
import { User } from '../models/User';
import moment from 'moment';
import key from '../config';

export class Servicos {
    public createToken(user:User){
        const payload= {
            sub:user.usuario,
            iat:moment().unix(),
            exp:moment().add(14,'days').unix(),
        }
        return jwt.encode(payload,key.llave.SECRET_TOKEN);
    }

}