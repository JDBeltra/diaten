// Definimos el enrutador
// Desde express voy a importar un metodo que me devvueleve un objeto y ahi pongo las rutas
import {Router} from 'express';
// Importar el controllador
import {indexController} from '../controllers/indexController';
class LoginRoute{
    public router:Router=Router();

    constructor(){
        this.config();
    }
    config():void{
        this.router.get('/inicio',indexController.dentro); //Estoy creando la ruta para inico  y voy a devolver hello
    }

}
const loginRoute=new LoginRoute();
export default loginRoute.router;