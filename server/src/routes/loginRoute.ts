// Definimos el enrutador
// Desde express voy a importar un metodo que me devvueleve un objeto y ahi pongo las rutas
import {Router} from 'express';
// Importar el controllador
import {loginController} from '../controllers/loginController';

class IndexRoutes{
    public router:Router=Router();
    constructor(){
        this.config();
    }
    config():void{
        this.router.post('/login/entrar',loginController.ingresar); //Esta ruta es para validar el ingreso
        this.router.post('/login/registrar',loginController.crearUsuario); //Esta ruta es para validar el ingreso
    }

}
const indexRoutes=new IndexRoutes();
export default indexRoutes.router;