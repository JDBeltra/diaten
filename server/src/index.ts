// creamos el @tipe de express npm i @types/express -D
import express , {Application} from 'express';
// Configuramos los modulos de morgan y demas npm i @types/morgan @types/cors
import morgan  from 'morgan';
import cors from 'cors';

import indexRoutes from './routes/indexRoute';
import loginRoute from './routes/loginRoute';




// Creamos la clase server que inicia el servidor
class Server{
    public app:Application;
    constructor(){
        // este construtor inicializa express
        this.app=express(); //Este devueleve un objeto la cual la almacenamos
        this.config();
        this.routes();
    }

    // Metodo que se encarga de configurar app
    config():void{
        this.app.set('port',process.env.PORT|| 3000); // Si hay un puerto definido tomalo o sino por el que le asigno
        this.app.use(morgan('dev')); //Permite ver las peticiones a la API
        this.app.use(cors()); //Ayuda a pedir los datos al servidor
        this.app.use(express.json()); //Permite aceptar formatos json
        this.app.use(express.urlencoded({extended:false})); //Por si queremos enviar desde un formulario html
    }
    // Metodo que permite definir de app las rutas al servidor
    routes():void{
        // Declaramos que vamos a usar el enrutador
        this.app.use(loginRoute)
        this.app.use(indexRoutes);
    }
    // Metodo start para inicializar el servidor
    start():void{
        this.app.listen(this.app.get('port'),()=>{
            console.log('Servidor de puerto por el', this.app.get('port')); //Corre por el puerto dado
        })
    }   
}

const server = new Server(); //Instanciamos la clase
server.start();

