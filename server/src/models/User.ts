// Creamos los eventos de hash
import * as CryptoJS from 'crypto-js';

export class User {
    // public id!:number
    public usuario: string;
    public password: string;
    private key = 'PyP San Gil';
    // public  persona_id!:number;

    constructor(usuario: string, password: string) {
        this.password = password;
        this.usuario = usuario;
        this.hashPassword();
    }
    hashPassword(): void {
            this.password=CryptoJS.AES.encrypt(this.password.trim(), this.key.trim()).toString();
    }

    public checkPassword(password2:string) {
        let ans1= CryptoJS.AES.decrypt(this.password.trim(), this.key.trim()).toString(CryptoJS.enc.Utf8);
        let ans2= CryptoJS.AES.decrypt(password2.trim(), this.key.trim()).toString(CryptoJS.enc.Utf8);
        // console.log('pass1'+ ans1+ ' pass2 '+ password2);
        if(ans1==ans2){
            return true
        }
        return false;
    }
    
}