"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Conexion a la base de datos 
const mysql_1 = __importDefault(require("mysql"));
const keys_1 = __importDefault(require("./keys"));
const pool = mysql_1.default.createPool(keys_1.default.database); //Crear el modulo de conexion a la base de datos
pool.getConnection(function (err, connection) {
    if (err) {
        switch (err.code) {
            case "PROTOCOL_CONNECTION_LOST":
                console.error('Conexion Perdida');
                break;
            case "ECONNREFUSED":
                console.error("Conexion Rechazada");
                break;
            case "ER_ACCESS_DENIED_ERROR":
                console.error('Error de credenciales');
                break;
            case "ER_CON_COUNT_ERROR":
                console.error('Demasiadas conexion');
                break;
            case "ECONNRESET":
                console.error('No se obtuvo respuesta del servidor');
                break;
        }
    }
    if (connection) {
        connection.release();
        console.log('DB CONECTADA');
    }
    return;
});
exports.default = pool;
