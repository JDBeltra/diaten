"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
// Creamos los eventos de hash
const CryptoJS = __importStar(require("crypto-js"));
class User {
    // public  persona_id!:number;
    constructor(usuario, password) {
        this.key = 'PyP San Gil';
        this.password = password;
        this.usuario = usuario;
        this.hashPassword();
    }
    hashPassword() {
        this.password = CryptoJS.AES.encrypt(this.password.trim(), this.key.trim()).toString();
    }
    checkPassword(password2) {
        let ans1 = CryptoJS.AES.decrypt(this.password.trim(), this.key.trim()).toString(CryptoJS.enc.Utf8);
        let ans2 = CryptoJS.AES.decrypt(password2.trim(), this.key.trim()).toString(CryptoJS.enc.Utf8);
        // console.log('pass1'+ ans1+ ' pass2 '+ password2);
        if (ans1 == ans2) {
            return true;
        }
        return false;
    }
}
exports.User = User;
