"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.loginController = void 0;
const database_1 = __importDefault(require("../database")); // importamos la conexion a ala base de datos
// Importamos el servico de tokens
const servicios_1 = require("../services/servicios");
// Importo la clase User
const User_1 = require("../models/User");
class LoginController {
    //Metodo para validar el ingreso
    ingresar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            // Creamos la instancia con el token
            const { username, password } = req.body;
            let user = new User_1.User(username, password); //Llenamos el constructor
            // Verificamos si el usuario existe y traemos la pass
            // console.log("El suaurio es: " + user.usuario + "  pass" + user.password);
            database_1.default.query("CALL existeUsuario (?)", [user.usuario], function (error, result) {
                if (error) {
                    res
                        .status(200)
                        .send({
                        token: null,
                        status: "Error no coincidencias" + error.code,
                    });
                }
                else {
                    // Si el resultado es mayor que cero
                    try {
                        if (Object.keys(result).length > 0) {
                            // Se compara las contreseñas
                            // res.send(user.password);
                            //   res.send(result[0][0]['@dbpass']);
                            // Comparamanos las contraseñas
                            if (user.checkPassword(result[0][0]["@dbpass"])) {
                                database_1.default.query("CALL Sesionar (?)", [user.usuario], function (err, respues) {
                                    if (err) {
                                        res
                                            .status(200)
                                            .send({
                                            token: null,
                                            status: "err de Sesionar" + err.code,
                                        });
                                    }
                                    else {
                                        if (Object.keys(respues).length > 0) {
                                            // res.status(200).send("Error de Sesionar");
                                            let service = new servicios_1.Servicos();
                                            res
                                                .status(200)
                                                .send({
                                                token: service.createToken(user),
                                                status: respues[0][0]["@salida"],
                                            });
                                        }
                                        else {
                                            res.status(200).send({ status: "No hay rol" });
                                        }
                                    }
                                });
                                //   Registro al procedure de sesionar
                                // res.send('Bienvenido Fulano de tal');
                            }
                            else {
                                res
                                    .status(200)
                                    .send({ token: null, status: "Contraseña errónea!" });
                            }
                        }
                    }
                    catch (error) {
                        res
                            .status(200)
                            .send({
                            token: null,
                            status: "Error no coincidencias" + error.code,
                        });
                    }
                }
            });
            // console.log(band);
            // if (band) {
            //     db.query("CALL Sesionar (?)", [user.usuario], function (err, respues) {
            //         if (err) {
            //             res.status(404).send("err de Sesionar" + err.code);
            //         } else {
            //             if (Object.keys(respues).length > 0) {
            //                 // res.status(404).send("Error de Sesionar");
            //                 res.send(respues[0][0]["@salida"]);
            //           }
            //         }
            //       });
            // }
            // console.log(user.password);
            // db.query('SET @val=0; SET @imprension="El usuario no existe"; CALL SesionarUsuario=(?,?,?,?)',[user.usuario,user.password,'@val','@impresion'], function (error, results){
            //     if (error) {
            //         res.status(404).send('ERROR EN LA CONSULTA O CONEXION' + error.code);
            //     } else {
            //         if (Object.keys(results).length > 0) {
            //         } else {
            //             res.status(404).json({text:'Usted no puede entrar'});
            //         }
            //     }
            // });
            // console.log(user.password);
            // db.query('call SesionarUsuario(?,?)', [user.usuario, user.password], function (error, results) {
            //     if (error) {
            //         res.status(404).send('ERROR EN LA CONSULTA O CONEXION' + error.code);
            //     } else {
            //         if (Object.keys(results).length > 0) {
            //             res.send(results);
            //         } else {
            //             res.status(404).send('Uste no puede entrar');
            //         }
            //     }
            // });
        });
    }
    Sesionar(user) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(user);
            database_1.default.query("CALL Sesionar (?)", [user.usuario], function (err, res) {
                if (err) {
                    res.status(404).send("Error de Sesionar");
                    res.status(404).send("Error de Sesionar" + err.code);
                }
                else {
                    if (Object.keys(res).length > 0) {
                        // res.send(res[0][0]["@salida"]);
                    }
                }
            });
        });
    }
    crearUsuario(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id, username, password, rol } = req.body;
            let user = new User_1.User(username, password); //Llenamos el constructor
            database_1.default.query("CALL CrearUsuario(?,?,?,?)", [id, user.usuario, user.password, rol], function (error, result) {
                if (error) {
                    res
                        .status(404)
                        .send({
                        'status': "ERROR EN LA CONSULTA O CONEXION" + error.code,
                        'token': null,
                    });
                }
                else {
                    if (Object.keys(result).length > 0) {
                        // res.send(result);
                        if (result[0][0]["@res"] != null) {
                            res.send({ 'status': 'Registrado a nombre de: ' + result[0][0]["@res"], 'token': 'si' });
                        }
                        else {
                            res.send({ 'status': result[0][0]["@salida"], 'token': 'no' });
                        }
                    }
                    else {
                        res.status(404).send({ 'status': 'Error de Creación de usuario', 'token': null });
                    }
                }
            }); //Consulta para verificar el usuario
        });
    }
}
exports.loginController = new LoginController();
