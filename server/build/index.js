"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// creamos el @tipe de express npm i @types/express -D
const express_1 = __importDefault(require("express"));
// Configuramos los modulos de morgan y demas npm i @types/morgan @types/cors
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const indexRoute_1 = __importDefault(require("./routes/indexRoute"));
const loginRoute_1 = __importDefault(require("./routes/loginRoute"));
// Creamos la clase server que inicia el servidor
class Server {
    constructor() {
        // este construtor inicializa express
        this.app = express_1.default(); //Este devueleve un objeto la cual la almacenamos
        this.config();
        this.routes();
    }
    // Metodo que se encarga de configurar app
    config() {
        this.app.set('port', process.env.PORT || 3000); // Si hay un puerto definido tomalo o sino por el que le asigno
        this.app.use(morgan_1.default('dev')); //Permite ver las peticiones a la API
        this.app.use(cors_1.default()); //Ayuda a pedir los datos al servidor
        this.app.use(express_1.default.json()); //Permite aceptar formatos json
        this.app.use(express_1.default.urlencoded({ extended: false })); //Por si queremos enviar desde un formulario html
    }
    // Metodo que permite definir de app las rutas al servidor
    routes() {
        // Declaramos que vamos a usar el enrutador
        this.app.use(loginRoute_1.default);
        this.app.use(indexRoute_1.default);
    }
    // Metodo start para inicializar el servidor
    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log('Servidor de puerto por el', this.app.get('port')); //Corre por el puerto dado
        });
    }
}
const server = new Server(); //Instanciamos la clase
server.start();
