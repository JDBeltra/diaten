"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Servicos = void 0;
const jwt_simple_1 = __importDefault(require("jwt-simple"));
const moment_1 = __importDefault(require("moment"));
const config_1 = __importDefault(require("../config"));
class Servicos {
    createToken(user) {
        const payload = {
            sub: user.usuario,
            iat: moment_1.default().unix(),
            exp: moment_1.default().add(14, 'days').unix(),
        };
        return jwt_simple_1.default.encode(payload, config_1.default.llave.SECRET_TOKEN);
    }
}
exports.Servicos = Servicos;
