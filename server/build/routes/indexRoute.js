"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Definimos el enrutador
// Desde express voy a importar un metodo que me devvueleve un objeto y ahi pongo las rutas
const express_1 = require("express");
// Importar el controllador
const indexController_1 = require("../controllers/indexController");
class LoginRoute {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/inicio', indexController_1.indexController.dentro); //Estoy creando la ruta para inico  y voy a devolver hello
    }
}
const loginRoute = new LoginRoute();
exports.default = loginRoute.router;
