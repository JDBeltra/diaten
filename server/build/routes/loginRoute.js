"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Definimos el enrutador
// Desde express voy a importar un metodo que me devvueleve un objeto y ahi pongo las rutas
const express_1 = require("express");
// Importar el controllador
const loginController_1 = require("../controllers/loginController");
class IndexRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/login/entrar', loginController_1.loginController.ingresar); //Esta ruta es para validar el ingreso
        this.router.post('/login/registrar', loginController_1.loginController.crearUsuario); //Esta ruta es para validar el ingreso
    }
}
const indexRoutes = new IndexRoutes();
exports.default = indexRoutes.router;
