import Swal from 'sweetalert2'


class Alertas {

    private bandera:boolean;

    public getBan(){
        return this.bandera;
    }
    public setBan(val:boolean){
        this.bandera=val;
    }

    public alert(titulo:string,texto:string,icono:any) {
        return Swal.fire({
            title: titulo,
            text: texto,
            icon: icono,
            showCancelButton:false,
            timer:3000
          });          
    }
    public confirmAlert(titulo:string,texto:string,icono:any,ban:boolean):any{
        Swal.fire({
            title:titulo,
            text:texto,
            icon:icono,
            showCancelButton:true,
            confirmButtonColor:'#057380',
            confirmButtonText:'Si!'
        }).then((result)=>{
            if(result.isConfirmed){
                return ban=true;
                Swal.fire(
                    'Listo',
                    'Se ha realizado con Exito',
                    'success'
                )
            }else{
                return ban=false;
            }
        })
    }
 }
 export const sweetAlert =new Alertas();