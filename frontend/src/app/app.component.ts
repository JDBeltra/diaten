import { Component } from "@angular/core";
import { Router } from "@angular/router";

declare var $: any;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "DiaTen";
  NotLogin = false;
  constructor(private router: Router) {
    // Navigate to /view
    // this.router.navigate(['/login'], { replaceUrl: true });
    // console.log("Esta es "+router.getCurrentNavigation().extras.state.someData);

    if (localStorage.getItem('token')) {
      this.NotLogin = true;
    } else {
      // console.log("Esta es " + router.url);
      this.NotLogin = false;
    }
    // console.log(window.location.href);
    // console.log('Esto es'+this.router.isActive('/login',this.NotLogin));  // to print only path eg:"/login"
  }
}
