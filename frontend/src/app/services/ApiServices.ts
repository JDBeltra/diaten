import { Injectable } from '@angular/core';
// Importamos el modulo de hacer las peticiones http
import {HttpClient} from '@angular/common/http';
// Modelo de login
import {Login} from '../models/login';
import { ResponseDb } from '../models/response.login';
import { User } from '../models/user';
@Injectable({
  providedIn: 'root'
})
export class LoginsService {

  API_URI='http://localhost:3000';
  constructor( private http:HttpClient) {}

  // Servicio para comunicarme con la API
  ingresar(inicio:Login){
    // console.log(`${this.API_URI}/login/entrar`);
    return this.http.post<ResponseDb>(`${this.API_URI}/login/entrar`, inicio);
    // return inicio;
  }
  crearUsuario(user:User){
    return this.http.post<ResponseDb>(`${this.API_URI}/login/registrar`, user);
  }
}
 