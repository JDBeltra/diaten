import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./vistas/login/login.component";
import { ControlPacienteComponent } from "./vistas/control-paciente/control-paciente.component";
import { DashboardComponent } from "./vistas/dashboard/dashboard.component";
import { SearchPacienteComponent } from "./vistas/search-paciente/search-paciente.component";
import { UsuarioPanelComponent } from "./vistas/usuario-panel/usuario-panel.component";
import { PruebasComponent } from "./pruebas/pruebas.component";
import { FileUploaderComponent } from './bloques/file-uploader/file-uploader.component';


const routes: Routes = [

  {
    path:"imposible",
    component:FileUploaderComponent,
  },
  {
    path: "index",
    component: DashboardComponent,
  },
  {
    path: "buscar",
    component: SearchPacienteComponent,
  },
  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "control",
    component: ControlPacienteComponent,
  },
  {
    path: "usuario-panel",
    component: UsuarioPanelComponent,
  },
  {
    path:"pruebas",
    component:PruebasComponent,
  },
  {
    path: "",
    redirectTo: "/index",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
export const routingComponents=[FileUploaderComponent, LoginComponent, DashboardComponent,SearchPacienteComponent,UsuarioPanelComponent,ControlPacienteComponent,PruebasComponent]
