import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { LoginsService } from "../../services/ApiServices";
import { Login } from "../../models/login";
import { Router } from "@angular/router";

import { ResponseDb } from "src/app/models/response.login";

// Alertas
import { sweetAlert } from "../../models/alertas";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  formLogin!: FormGroup;
  constructor(private loginService: LoginsService, private router: Router) {}

  errorStatus: boolean = false;
  errorMsj = "";

  ngOnInit() {
    // Checkear si ya inicio sesion
    this.checkLocalStorage();

    // Formulario de inicio de Sesion
    this.formLogin = new FormGroup({
      username: new FormControl(null, [
        Validators.required,
        Validators.minLength(3),
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(3),
      ]),
    });

    // Eventos para el login
    const inputs = document.querySelectorAll(".input");

    function addcl() {
      let parent = this.parentNode.parentNode;
      parent.classList.add("focus");
    }

    function remcl() {
      let parent = this.parentNode.parentNode;
      if (this.value == "") {
        parent.classList.remove("focus");
      }
    }

    inputs.forEach((input) => {
      input.addEventListener("focus", addcl);
      input.addEventListener("blur", remcl);
    });
  }
  onSubmit(login: Login) {
    // console.log(login);
    // //Las constnantes de las credenciales
    // const nombre = this.formLogin.value.username;
    // const pw = this.formLogin.value.password;
    // //Creamos el objeto
    // login = {
    //   password: pw,
    //   username: nombre, 
    // };
    // const login:Login=new Login(nombre,pw);
    //Llamanos el servicio

    this.loginService.ingresar(login).subscribe(
      (data) => {
        let rol = null;
        try {
        let respuesta: ResponseDb = data;
          // console.log(respuesta.status);
          // console.log(respuesta.token);
          if (respuesta.token != null) {
            switch (respuesta.status) {
              case "1":
                rol = "Doctor";
                break;
              case "2":
                rol = "Enfermera";
                break;
              case "3":
                rol = "Jefe de Enfermeria";
                break;
              case "4":
                rol = "Administrador";
                break;
              default:
                break;
            }
            // Almacenamos el token en el equipo
            localStorage.setItem("token", respuesta.token);
            let texto = "El usuario es de rol: " + rol;

            localStorage.setItem("user",login.username);
            localStorage.setItem("pw",login.password);
            localStorage.setItem("rol",rol);

            sweetAlert.alert("Bienvenido!", texto, "success");
            // //Realizamos la redireccion
            this.router.navigate(["index"]);
          }else{
            this.errorStatus=true;
            this.errorMsj=respuesta.status;
          }
        } catch (error) {}
      },
      (err) => {
        sweetAlert.alert(
          "Error!",
          "Por favor verifique los datos ingresados",
          "error"
        );
      }
    );
    // console.log("Funciona");
  }
  checkLocalStorage(){
    if(localStorage.getItem('token')){
      this.router.navigateByUrl('index');
    }
  }
}
