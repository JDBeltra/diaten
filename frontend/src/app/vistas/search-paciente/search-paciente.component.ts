import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-paciente',
  templateUrl: './search-paciente.component.html',
  styleUrls: ['./search-paciente.component.css']
})
export class SearchPacienteComponent implements OnInit {

  constructor() { }
  // El titulo del componente
  titulo="Buscar Paciente";
  ngOnInit() {
  }

}
