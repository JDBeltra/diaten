import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { sweetAlert } from "src/app/models/alertas";
import { User } from "src/app/models/user";
import { LoginsService } from "src/app/services/ApiServices";
import { ResponseDb } from "src/app/models/response.login";

@Component({
  selector: "app-usuario-panel",
  templateUrl: "./usuario-panel.component.html",
  styleUrls: ["./usuario-panel.component.css"],
})
export class UsuarioPanelComponent implements OnInit {
  formUser!: FormGroup;
  closeResult: string;
  imagePatch:string="../../../assets/noPhoto.png";

  constructor(private modalService: NgbModal, private userService:LoginsService) {}

  newUser(content) {
    this.modalService
      .open(content, { ariaLabelledBy: "modal-basic-title", size:'lg', centered:true })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit() {
    this.formUser=new FormGroup({
      nombre: new FormControl(null,[Validators.required,Validators.minLength(4)]),
      pass:new FormControl(null,[Validators.required,Validators.minLength(5)]),
      pass1:new FormControl(null,[Validators.required,Validators.minLength(5)]),
      rol:new FormControl(null,[Validators.required]),
      id:new FormControl(null, [Validators.required,Validators.minLength(5)])
    })
  }
  createUser(user:User){
    if(this.formUser.get('pass').value==this.formUser.get('pass1').value){
        user.id= this.formUser.get('id').value;
        user.username=this.formUser.get('nombre').value;
        user.password=this.formUser.get('pass').value;
        user.rol=this.formUser.get('rol').value;

        console.log(user);
        
        // user.urlPath=this.formUser.get('urlPath').value;
        // Hacemos la peticion de creacion
        this.userService.crearUsuario(user).subscribe(
          (data)=>{
            let res:ResponseDb=data;
            console.log("ayuda");
            if(res.token=='si'){
              sweetAlert.alert('Exito de creación de usuario!',res.status,'success');
            }else{
              sweetAlert.alert('Error de Usuario',res.status,'error');
            }
          },
          (err)=>{
            sweetAlert.alert(
              "Error!",
              err,
              "error"
            );
          }
        )
    }else{
      sweetAlert.alert('Error!','Las contraseñas no coinciden!','error');

    }
  }
  // Evento para subir la imagen
  onFileSelect(file: Event) {
    this.formUser.patchValue({ urlPath: file });
    this.formUser.get('urlPath').updateValueAndValidity();
  }
}
