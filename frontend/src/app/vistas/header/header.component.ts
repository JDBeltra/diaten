import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import * as $ from "jquery";
import { ResponseDb } from "src/app/models/response.login";
import { LoginsService } from "src/app/services/ApiServices";
import { sweetAlert } from "../../models/alertas";
@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent {
  constructor(private loginService: LoginsService, private router: Router) {}
  titulo: string = "DIATEN";

  siderCollapse() {
    $("#navbar").toggleClass("active");
    $("#sidebarCollapse").toggleClass("active");
  }
  opacidad() {
    // hide sidebar
    // $(".normal").toggleClass("opacidad");
    // $(".normal").removeClass("normal");
    // $(this).toggleClass("normal");
    // // hide overlay
    // $('.overlay').removeClass('active');
  }
  ngOnInit() {
    this.checkLocalStorage();
  }
  cerrarSesion() {
    let datos = {
      username: localStorage.getItem("user"),
      password: localStorage.getItem("pw"),
    };
    let msj = sweetAlert;
    this.loginService.ingresar(datos).subscribe(
      (data) => {
        let respuesta: ResponseDb = data;
        try {
          console.log("llega" + respuesta);
          if (respuesta.status == "CloseSesion") {
            msj.alert(
              "Ha cerrado Sesión!",
              "Ha cerrado su sesión con exito",
              "success"
            );
            localStorage.removeItem("user");
            localStorage.removeItem("pw");
            localStorage.removeItem("token");
            localStorage.removeItem("rol");

            //Realizamos la redireccion
            // this.router.navigate(["login"]);
            this.router.navigateByUrl("login");
          }
        } catch (error) {
          console.log("Error de base de datos");
        }
      },
      (err) => {
        sweetAlert.alert(
          "Error!",
          "Por favor verifique los datos ingresados",
          "error"
        );
        console.log(err);
      }
    );
  }
  checkLocalStorage() {
    if (localStorage.getItem("token")) {
    } else {
      this.router.navigateByUrl("login");
    }
  }
}
