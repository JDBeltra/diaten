import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlPacienteComponent } from './control-paciente.component';

describe('ControlPacienteComponent', () => {
  let component: ControlPacienteComponent;
  let fixture: ComponentFixture<ControlPacienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlPacienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlPacienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
